import React, {useContext, useEffect, useMemo} from 'react';
import {
  Container,
  Body,
  LocationContainer,
  Title,
  DetailsContainer,
  IconContainer,
  List,
  ItemContainer,
  ItemTitle,
} from './styles';
import WeatherIcon from '../../components/weather-icon';
import WeatherContext from '../../../domain/context/Weather';

type DetailsProps = {
  index: number;
  navigation: any;
};

type Option = {
  label: string;
  value: string;
};

const Details: React.FC<DetailsProps> = ({index, navigation}) => {
  const {
    state: {weathers},
  } = useContext(WeatherContext);

  useEffect(() => {
    const weather = weathers[index];
    navigation.setOptions({title: weather.cityName});
  }, []);

  const data = useMemo(() => {
    const weather = weathers[index];
    return [
      {label: 'Temperatura máxima', value: weather.maxTemperature, id: '1'},
      {label: 'Temperatura mínima', value: weather.minTemperature, id: '2'},
      {label: 'Vento', value: weather.windSpeed, id: '3'},
      {label: 'Humidade', value: weather.humidity, id: '4'},
      {label: 'Latitude', value: weather.latitude, id: '5'},
      {label: 'Longitude', value: weather.longitude, id: '6'},
    ];
  }, [weathers, index]);

  return (
    <Container>
      <Body>
        <LocationContainer>
          <DetailsContainer>
            <Title bold size={35}>
              {weathers[index].description}
            </Title>
            <Title bold size={50} numberOfLines={1}>
              {`${weathers[index].temperature}°`}
            </Title>
          </DetailsContainer>
          <IconContainer>
            <WeatherIcon name={weathers[index].icon} />
          </IconContainer>
        </LocationContainer>
        <List<any>
          data={data}
          keyExtractor={(item: any) => item.id}
          renderItem={({item}: {item: Option}) => (
            <ItemContainer>
              <ItemTitle flex={7} numberOfLines={1}>
                {item.label}
              </ItemTitle>
              <ItemTitle flex={3} numberOfLines={1}>
                {item.value}
              </ItemTitle>
            </ItemContainer>
          )}
        />
      </Body>
    </Container>
  );
};

export default Details;
