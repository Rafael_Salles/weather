import React, {createContext, useState} from 'react';
import {Weather} from '../../entities/weather';

type WeatherType = {
  weathers: Weather[];
};

type PropsWeatherContext = {
  state: WeatherType;
  setState: React.Dispatch<React.SetStateAction<WeatherType>>;
};

const DEFAULT_VALUE = {
  state: {
    weathers: [],
  },
  setState: () => {},
};

const WeatherContext = createContext<PropsWeatherContext>(DEFAULT_VALUE);

const WeatherProvider: React.FC = ({children}) => {
  const [state, setState] = useState<WeatherType>(DEFAULT_VALUE.state);

  return (
    <WeatherContext.Provider
      value={{
        state,
        setState,
      }}>
      {children}
    </WeatherContext.Provider>
  );
};

export {WeatherProvider};
export default WeatherContext;
