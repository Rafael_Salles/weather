import React from 'react';
import WeatherIcon from '../weather-icon';
import {Container, CityTitle, WeatherContainer, WeatherTitle} from './styles';

type CityCardProps = {
  name: string;
  temperature: string;
  icon:
    | '01d'
    | '01n'
    | '02d'
    | '02n'
    | '03d'
    | '03n'
    | '04d'
    | '04n'
    | '09d'
    | '09n'
    | '10d'
    | '10n'
    | '11d'
    | '11n'
    | '13d'
    | '13n'
    | '50d'
    | '50n';
  callback: () => void;
};

const CityCard: React.FC<CityCardProps> = ({
  name,
  temperature,
  icon,
  callback,
}) => {
  return (
    <Container onPress={callback}>
      <CityTitle numberOfLines={1}>{name}</CityTitle>
      <WeatherContainer>
        <WeatherIcon name={icon} />
      </WeatherContainer>
      <WeatherTitle numberOfLines={1}>{temperature}°</WeatherTitle>
    </Container>
  );
};

export default CityCard;
