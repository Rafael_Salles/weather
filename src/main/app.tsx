import React from 'react';
import {WeatherProvider} from '../domain/context/Weather';
import {NavigationContainer} from '@react-navigation/native';
import AppStack from './navigators/appstack';

const App = () => {
  return (
    <WeatherProvider>
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </WeatherProvider>
  );
};

export default App;
