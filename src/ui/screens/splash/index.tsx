import React, {useContext, useEffect} from 'react';
import {Animation, Container} from './styles';
import ClearSky from '../../assets/animations/day-clear-sky.json';
import {LoadWeathers} from '../../../main/protocols/use-cases/load-weathers';
import WeatherContext from '../../../domain/context/Weather';

type SplashProps = {
  loadWeathers: LoadWeathers;
  navigation: any;
};

const Splash: React.FC<SplashProps> = ({loadWeathers, navigation}) => {
  const {setState} = useContext(WeatherContext);

  useEffect(() => {
    const loadAllWeathers = async () => {
      const weathers = await loadWeathers.handle();
      setState({weathers});
      navigation.replace('Home');
    };
    loadAllWeathers();
  }, [loadWeathers, navigation, setState]);

  return (
    <Container>
      <Animation source={ClearSky} autoPlay loop />
    </Container>
  );
};

export default Splash;
