import {iconName} from '../../../domain/entities/icon';
import {Weather} from '../../../domain/entities/weather';

export interface LoadWeathers {
  handle: () => Promise<Weather[]>;
}

export namespace LoadWeathers {
  export type DTO = {
    id: number;
    name: string;
    weather: Array<{
      main: string;
      description: string;
      icon: iconName;
    }>;
    main: {
      temp: number;
      feels_like: number;
      temp_min: number;
      temp_max: number;
      humidity: number;
    };
    wind: {
      speed: number;
    };
    coord: {
      lat: number;
      lon: number;
    };
  };
}
