import styled from 'styled-components/native';

type TitleProps = {
  bold?: boolean;
  size?: number;
};

type ItemTitleProps = {
  flex: number;
};

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const Body = styled.View`
  background-color: #fff;
  flex: 1;
  padding-horizontal: 15px;
`;

export const LocationContainer = styled.View`
  margin-vertical: 15px;
  flex-direction: row;
  align-items: center;
`;

export const DetailsContainer = styled.View`
  flex: 1;
`;

export const IconContainer = styled.View`
  flex: 1;
  align-items: center;
`;

export const Title = styled.Text<TitleProps>`
  text-align: center;
  color: #0a193a;
  font-size: ${props => props.size ?? '15'}px;
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
`;

export const List = styled.FlatList``;

export const ItemContainer = styled.View`
  flex: 1;
  flex-direction: row;
`;

export const ItemTitle = styled.Text<ItemTitleProps>`
  flex: ${props => props.flex};
  color: #0a193a;
  font-size: 18px;
  font-weight: bold;
`;
