import {iconName} from './icon';

export type Weather = {
  cityName: string;
  temperature: number;
  maxTemperature: number;
  minTemperature: number;
  windSpeed: number;
  humidity: number;
  description: string;
  icon: iconName;
  latitude: number;
  longitude: number;
};
