# Weather

Desafio React Native.

## Instalação

Utilizar o npm para instalar as dependências.

```bash
npm install
```

## Detalhes do padrão arquitetural utilizado

Padrões de projeto utilizados para criar essa estrutura: Adapter e Factory.  
Inversão de dependência e injeção de dependência também foram utilizados, especialmente para desacoplar classes que não devem "se conhecer".

- Infra - Toda comunicação externa ao aplicativo. Por exemplo: HttpClient, Storage, Push Notification...
- Domain - Regras de negócio da aplicação.
- UI - Interface de usuário.
- Main - Camada mais abstrada, que conhece e conecta uma camada a outra.

## Contribuidores

- [Rafael Marques](https://www.linkedin.com/in/rafael-marques-2232a1126/)

## Observações

- Não consegui desenvolver testes dentro do prazo, mas estava previsto no meu escopo inicial.
