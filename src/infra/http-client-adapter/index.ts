import axios, {AxiosResponse} from 'axios';
import {
  HttpClient,
  HttpRequest,
  HttpResponse,
} from '../../main/protocols/http-client';

export class HttpClientAdapter implements HttpClient {
  async request(data: HttpRequest): Promise<HttpResponse> {
    let axiosResponse: AxiosResponse;
    try {
      axiosResponse = await axios.request({
        url: data.url,
        method: data.method,
        data: data.body,
        headers: data.headers,
      });
    } catch (error) {
      axiosResponse = error.response;
    }
    return {
      statusCode: axiosResponse.status,
      body: axiosResponse.data,
    };
  }
}

export default new HttpClientAdapter();
