import {
  GeolocationClient,
  GeolocationResponse,
} from '../../main/protocols/geolocation-client';
import Geolocation from '@react-native-community/geolocation';

export class GeolocationClientAdapter implements GeolocationClient {
  requestGeolocation(): Promise<GeolocationResponse> {
    return new Promise(resolve => {
      Geolocation.getCurrentPosition(
        ({coords}) => {
          resolve({
            statusCode: 0,
            coords: {
              latitude: coords.latitude,
              longitude: coords.longitude,
            },
          });
        },
        err => {
          resolve({
            statusCode: err.code,
          });
        },
        {
          timeout: 3000,
          enableHighAccuracy: true,
        },
      );
    });
  }
}

export default new GeolocationClientAdapter();
