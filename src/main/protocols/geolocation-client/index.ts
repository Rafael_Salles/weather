export interface GeolocationClient {
  requestGeolocation: () => Promise<GeolocationResponse>;
}

export type GeolocationResponse = {
  statusCode: number;
  coords?: {
    latitude: number;
    longitude: number;
  };
};
