import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import RemoteLoadWeathersCoords from '../../domain/use-cases/remote-load-weathers-coords';
import geolocationClientAdapter from '../../infra/geolocation-client-adapter';
import httpClientAdapter from '../../infra/http-client-adapter';
import Home from '../../ui/screens/home';
import {AppStackParamList} from '../navigators/appstack';

const remoteLoadWeathers = new RemoteLoadWeathersCoords(httpClientAdapter);

type Props = NativeStackScreenProps<AppStackParamList, 'Home'>;

const MakeHome: React.FC<Props> = ({navigation}) => {
  return (
    <Home
      loadWeathersCoords={remoteLoadWeathers}
      geolocation={geolocationClientAdapter}
      navigation={navigation}
    />
  );
};

export default MakeHome;
