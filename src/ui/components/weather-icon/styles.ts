import styled from 'styled-components/native';
import Lottie from 'lottie-react-native';

type AnimationProps = {
  height?: number;
  width?: number;
};

export const Animation = styled(Lottie)<AnimationProps>`
  flex: 1;
  width: 100%;
`;
