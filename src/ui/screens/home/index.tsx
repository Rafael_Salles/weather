import React, {useContext, useEffect} from 'react';
import {Container, List} from './styles';
import CityCard from '../../components/city-card';
import WeatherContext from '../../../domain/context/Weather';
import {GeolocationClient} from '../../../main/protocols/geolocation-client';
import {LoadWeathersCoords} from '../../../main/protocols/use-cases/load-weathers-coords';
import {Weather} from '../../../domain/entities/weather';

type HomeProps = {
  geolocation: GeolocationClient;
  loadWeathersCoords: LoadWeathersCoords;
  navigation: any;
};

const Home: React.FC<HomeProps> = ({
  geolocation,
  loadWeathersCoords,
  navigation,
}) => {
  const {
    state: {weathers},
    setState,
  } = useContext(WeatherContext);

  useEffect(() => {
    const getWeatherByLocation = async () => {
      const response = await geolocation.requestGeolocation();
      if (response.statusCode === 0) {
        const {coords} = response;
        const weather = await loadWeathersCoords.handle(
          coords!.latitude,
          coords!.longitude,
        );
        setState({weathers: [weather, ...weathers]});
      }
    };
    getWeatherByLocation();
  }, []);

  return (
    <Container>
      <List<any>
        data={weathers}
        keyExtractor={(item: Weather, index: number) => index.toString()}
        renderItem={({item, index}: {item: Weather; index: number}) => (
          <CityCard
            name={item.cityName}
            temperature={item.temperature.toString()}
            icon={item.icon}
            callback={() => navigation.push('Details', {index})}
          />
        )}
      />
    </Container>
  );
};

export default Home;
