import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import Details from '../../ui/screens/details';
import {AppStackParamList} from '../navigators/appstack';

type Props = NativeStackScreenProps<AppStackParamList, 'Details'>;

const MakeSplash: React.FC<Props> = ({navigation, route}) => {
  const {index} = route.params;
  return <Details navigation={navigation} index={index} />;
};

export default MakeSplash;
