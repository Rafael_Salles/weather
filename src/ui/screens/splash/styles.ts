import styled from 'styled-components/native';
import Lottie from 'lottie-react-native';

export const Container = styled.View`
  background-color: #fff;
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Animation = styled(Lottie)`
  height: 400px;
  width: 400px;
`;
