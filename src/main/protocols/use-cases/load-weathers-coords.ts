import {iconName} from '../../../domain/entities/icon';
import {Weather} from '../../../domain/entities/weather';

export interface LoadWeathersCoords {
  handle: (latitude: number, longitude: number) => Promise<Weather>;
}

export namespace LoadWeathersCoords {
  export type DTO = {
    id: number;
    name: string;
    weather: Array<{
      main: string;
      description: string;
      icon: iconName;
    }>;
    main: {
      temp: number;
      feels_like: number;
      temp_min: number;
      temp_max: number;
      humidity: number;
    };
    wind: {
      speed: number;
    };
    coord: {
      lat: number;
      lon: number;
    };
  };
}
