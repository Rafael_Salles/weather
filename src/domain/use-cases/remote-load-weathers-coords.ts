import {Weather} from '../entities/weather';
import {HttpClient} from '../../main/protocols/http-client';
import {LoadWeathersCoords} from '../../main/protocols/use-cases/load-weathers-coords';

export default class RemoteLoadWeathersCoords implements LoadWeathersCoords {
  constructor(
    private readonly httpClient: HttpClient<LoadWeathersCoords.DTO>,
  ) {}

  async handle(latitude: number, longitude: number): Promise<Weather> {
    const url = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=d2797255b3bd51f73590d78a87cdd319&units=metric&lang=pt_br`;
    const response = await this.httpClient.request({
      url: url,
      method: 'get',
    });
    return {
      cityName: response.body!.name,
      temperature: response.body!.main.temp,
      maxTemperature: response.body!.main.temp_max,
      minTemperature: response.body!.main.temp_min,
      windSpeed: response.body!.wind.speed,
      humidity: response.body!.main.humidity,
      description: response.body!.weather[0]?.description,
      icon: response.body!.weather[0]!.icon,
      latitude: response.body!.coord.lat,
      longitude: response.body!.coord.lon,
    };
  }
}
