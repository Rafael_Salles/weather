import React, {useMemo} from 'react';
import {iconName} from '../../../domain/entities/icon';
import ClearSkyDay from '../../assets/animations/01d.json';
import ClearSkyNight from '../../assets/animations/01n.json';
import FewCloudsDay from '../../assets/animations/02d.json';
import FewCloudsNight from '../../assets/animations/02n.json';
import ScatteredCloudsDay from '../../assets/animations/03d.json';
import ScatteredCloudsNight from '../../assets/animations/03n.json';
import BrokenCloudsDay from '../../assets/animations/04d.json';
import BrokenCloudsNight from '../../assets/animations/04n.json';
import ShowerRainDay from '../../assets/animations/09d.json';
import ShowerRainNight from '../../assets/animations/09n.json';
import RainDay from '../../assets/animations/10d.json';
import RainNight from '../../assets/animations/10n.json';
import ThunderstormDay from '../../assets/animations/11d.json';
import ThunderstormNight from '../../assets/animations/11n.json';
import SnowDay from '../../assets/animations/13d.json';
import SnowNight from '../../assets/animations/13n.json';
import MistDay from '../../assets/animations/50d.json';
import MistNight from '../../assets/animations/50n.json';
import {Animation} from './styles';

type WeatherIconProps = {
  name: iconName;
};

const WeatherIcon: React.FC<WeatherIconProps> = ({name}) => {
  const icon = useMemo(() => {
    switch (name) {
      case '01d':
        return ClearSkyDay;
      case '01n':
        return ClearSkyNight;
      case '02d':
        return FewCloudsDay;
      case '02n':
        return FewCloudsNight;
      case '03d':
        return ScatteredCloudsDay;
      case '03n':
        return ScatteredCloudsNight;
      case '04d':
        return BrokenCloudsDay;
      case '04n':
        return BrokenCloudsNight;
      case '09d':
        return ShowerRainDay;
      case '09n':
        return ShowerRainNight;
      case '10d':
        return RainDay;
      case '10n':
        return RainNight;
      case '11d':
        return ThunderstormDay;
      case '11n':
        return ThunderstormNight;
      case '13d':
        return SnowDay;
      case '13n':
        return SnowNight;
      case '50d':
        return MistDay;
      case '50n':
        return MistNight;
    }
  }, [name]);

  return <Animation source={icon} autoPlay loop />;
};

export default WeatherIcon;
