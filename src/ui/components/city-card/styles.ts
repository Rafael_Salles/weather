import styled from 'styled-components/native';

export const Container = styled.TouchableOpacity`
  border-width: 0.5px;
  border-color: #0a193a;
  padding: 15px;
  flex-direction: row;
  align-items: center;
  height: 80px;
`;

export const CityTitle = styled.Text`
  text-align: center;
  flex: 4;
  color: #0a193a;
  font-size: 20px;
  font-weight: bold;
`;

export const WeatherContainer = styled.View`
  flex: 4;
  justify-content: center;
  align-items: center;
`;

export const WeatherTitle = styled.Text`
  text-align: center;
  flex: 2;
  color: #0a193a;
  font-size: 20px;
  font-weight: bold;
`;
