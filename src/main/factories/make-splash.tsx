import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import RemoteLoadWeathers from '../../domain/use-cases/remote-load-weathers';
import httpClientAdapter from '../../infra/http-client-adapter';
import Splash from '../../ui/screens/splash';
import {AppStackParamList} from '../navigators/appstack';

const remoteLoadWeathers = new RemoteLoadWeathers(httpClientAdapter);

type Props = NativeStackScreenProps<AppStackParamList, 'Splash'>;

const MakeSplash: React.FC<Props> = ({navigation}) => {
  return <Splash loadWeathers={remoteLoadWeathers} navigation={navigation} />;
};

export default MakeSplash;
