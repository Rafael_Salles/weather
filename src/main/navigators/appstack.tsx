import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MakeDetails from '../factories/make-details';
import MakeSplash from '../factories/make-splash';
import MakeHome from '../factories/make-home';

export type AppStackParamList = {
  Splash: undefined;
  Home: undefined;
  Details: {
    index: number;
  };
};

const Stack = createNativeStackNavigator<AppStackParamList>();

function AppStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={MakeSplash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Home"
        component={MakeHome}
        options={{headerShown: false}}
      />
      <Stack.Screen name="Details" component={MakeDetails} />
    </Stack.Navigator>
  );
}

export default AppStack;
