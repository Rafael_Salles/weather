import {Weather} from '../entities/weather';
import {HttpClient} from '../../main/protocols/http-client';
import {LoadWeathers} from '../../main/protocols/use-cases/load-weathers';
import cities from '../../main/json/cities.json';

export default class RemoteLoadWeathers implements LoadWeathers {
  constructor(private readonly httpClient: HttpClient<LoadWeathers.DTO>) {}

  async handle(): Promise<Weather[]> {
    const urls = cities.map(
      e =>
        `https://api.openweathermap.org/data/2.5/weather?id=${e}&appid=d2797255b3bd51f73590d78a87cdd319&units=metric&lang=pt_br`,
    );
    const promises = urls.map(e =>
      this.httpClient.request({
        url: e,
        method: 'get',
      }),
    );
    const response = await Promise.all(promises);
    return response.map(e => ({
      cityName: e.body!.name,
      temperature: e.body!.main.temp,
      maxTemperature: e.body!.main.temp_max,
      minTemperature: e.body!.main.temp_min,
      windSpeed: e.body!.wind.speed,
      humidity: e.body!.main.humidity,
      description: e.body!.weather[0]?.description,
      icon: e.body!.weather[0]!.icon,
      latitude: e.body!.coord.lat,
      longitude: e.body!.coord.lon,
    }));
  }
}
